# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-10-08 17:43
from __future__ import unicode_literals

from django.db import migrations

PROTECTED_LABELS = [
    'H1OK',
    'H1NO',
    'L1OK',
    'L1NO',
    'V1OK',
    'V1NO',
    'ADVOK',
    'ADVNO',
]

def protect_labels(apps, schema_editor):
    Label = apps.get_model('events', 'Label')

    for label in PROTECTED_LABELS:
        l = Label.objects.get(name=label)
        l.protected = True
        l.save(update_fields=['protected'])


def unprotect_labels(apps, schema_editor):
    Label = apps.get_model('events', 'Label')

    for label in PROTECTED_LABELS:
        l = Label.objects.get(name=label)
        l.protected = False
        l.save(update_fields=['protected'])


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0027_label_protected'),
    ]

    operations = [
        migrations.RunPython(protect_labels, unprotect_labels),
    ]
