# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-19 19:15
from __future__ import unicode_literals

from django.db import migrations, models

# Add initial auth groups for tiered access and permissions

# Group names
GROUPS = [
    'executives',
    'Communities:LSCVirgoLIGOGroupMembers',
    'gw-astronomy:LV-EM',
    'public_users',
    'h1_control_room',
    'l1_control_room',
    'v1_control_room',
    'gw-astronomy:LV-EM:Observers',
    'em_advocates',
]

def add_groups(apps, schema_editor):
    Group = apps.get_model('auth', 'Group')

    for group_name in GROUPS:
        group, created = Group.objects.get_or_create(name=group_name)

def remove_groups(apps, schema_editor):
    Group = apps.get_model('auth', 'Group')

    for group_name in GROUPS:
        try:
            group = Group.objects.get(name=group_name)
        except Group.DoesNotExist:
            print("Error getting group {0} to delete, skipping".format(
                group_name))
            break
        group.delete()

class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0009_set_auth_user_charset'),
    ]

    operations = [
        migrations.RunPython(add_groups, remove_groups)
    ]
